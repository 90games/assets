﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public abstract class behavior_Default : AI {

    //temporary
    public bool triggerUpdatePath=false;
    public bool localAvoidance = false;

	// Use this for initialization
	new protected void Start () {
        this.behavior = "Default";
        base.Start();
        //this.destination = RandomPointOnCircle(this.targetNode.transform.position, this.targetNode.GetComponent<NodeControl>().capRadius);
        //if (destination == Vector3.zero)
        //{
        //    this.destination = RandomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
        //    if (destination == Vector3.zero)
        //    {
        //        Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0), even after attempted fix!");
        //    }
        //}
        seeker.StartPath(this.transform.position, this.destination, OnPathComplete);
        StartCoroutine(UpdatePath());
        StartCoroutine(AcquireTarget());
    }

    // Update is called once per frame
    new protected void Update () {
        base.Update();
        if (triggerUpdatePath)
        {
            triggerUpdatePath = false;
            TestUpdatePath();
        }
        if (destination == Vector3.zero || (targetNode.transform.position- destination).magnitude>targetNode.GetComponent<NodeControl>().capRadius)
        {
            this.destination = (Vector3)RandomNodeOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius).position;
            if (destination == Vector3.zero)
            {
                Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0), even after attempted fix!");
            }
        }

        if (this.target != null)
        {
            base.Engage();
        }
        else
        {
            if (path != null)
            {
                if (path.vectorPath != null)
                {
                    if (currentWaypoint >= path.vectorPath.Count)
                    {
                        if (!pathIsEnded)
                        {
                            Debug.Log(name + " has reached end of path!");
                            pathIsEnded = true;
                        }
                    }
                    else {
                        pathIsEnded = false;
                        MoveTo(path.vectorPath[currentWaypoint]);
                        if ((path.vectorPath[currentWaypoint] - transform.position).sqrMagnitude < 0.2f)
                        {
                            currentWaypoint++;
                        }
                    }
                }
            }
        }
    }

    //temporary
    public void TestUpdatePath()
    {
        //if (target != null)
        //{
        //    seeker.StartPath(this.transform.position, target.transform.position, OnPathComplete);
        //}
        //else if (destination == Vector3.zero)
        //{
        //    Debug.LogError("Unit " + this.name + " has null destination");
        //}
        //else {
        //    Debug.Log("Updating path with destination " + this.destination.ToString());
        //    StartCoroutine(ForceClearSelfoccupiedCR(0.2f));
        //    seeker.StartPath(this.transform.position, this.destination, OnPathComplete);
        //}
        //return;
    }

    private IEnumerator UpdatePath()
    {
        while (true)
        {
            if (target != null)
            {
                seeker.StartPath(this.transform.position, target.transform.position, OnPathComplete);
            }
            else if (destination == Vector3.zero)
            {
                Debug.LogError("Unit " + this.name + " has null destination");
                yield return false;
            }
            else {
                Debug.Log("Updating path with destination " + this.destination.ToString());
                if (localAvoidance)
                {
                    ForceSetOccupiedUnwalkable();
                    for(int i=1; i< path.vectorPath.Count; i++)
                    {
                        Debug.DrawLine(path.vectorPath[i], path.vectorPath[i - 1], Color.red, 0.5f);
                    }
                }
                yield return new WaitForEndOfFrame();
                //seeker.StartPath((Vector3)((GridNode)NearestGraphNode()).position, this.destination, OnPathComplete);
                seeker.StartPath(transform.position, this.destination, OnPathComplete);
                //yield return new WaitForEndOfFrame();
                //yield return new WaitForEndOfFrame();
                //yield return new WaitForEndOfFrame();
                GameController.GridScan();
                if (!localAvoidance) yield return new WaitForSeconds(Random.value);
            }
            yield return new WaitForSeconds(0.5f/*+Random.value*/);
        }
    }

    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            this.path = p;
            currentWaypoint = 0;
        }
        else Debug.LogError("BAD PATH");
    }
}