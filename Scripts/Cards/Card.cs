﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SpriteRenderer))]

public abstract class Card {

    //Public:
    public int teamID = 0;
    public string cardName;
    public int eCost;
    public Player owner;
    public bool used = false;

    //Temporary
    public Vector3 showMouse;

    //Private:
    public bool isReady;     
    public Vector3 spawnHere;
    public GameObject goHere;


    public virtual void Activate()
    {
        if (owner.energy < eCost) return;
        owner.energy -= eCost;
        //for(int i=0; i<owner.hand.Count; i++)
        //{
        //    if (owner.hand[i] == this) owner.hand.RemoveAt(i); 
        //}
        sendToGraveyard();
    }

    protected virtual void sendToGraveyard()
    {
        used = true;
        //Card newCard = new Card();
        //newCard = this;
        //GameController.teamGraveyards[teamID].Add(newCard);
    }
}
