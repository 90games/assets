﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnBaneling : card_SpawnUnitGeneric {
    
    public card_SpawnBaneling()
    {
        cardName = "Spawn Baneling";
        prefab = GameController.GetPrefab<unit_Baneling10>();
        squadSize = 1;
        eCost = 3;
    }
}
