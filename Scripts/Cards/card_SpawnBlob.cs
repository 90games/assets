﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnBlob : card_SpawnUnitGeneric {

    public card_SpawnBlob()
    {
        cardName = "Spawn Blob";
        prefab = GameController.GetPrefab<unit_Blob>();
        squadSize = 1;
        eCost = 7;
    }
}
