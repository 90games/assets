﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnJailbreak : card_SpawnUnitGeneric {

    public card_SpawnJailbreak()
    {
        cardName = "Spawn Jailbreak";
        prefab = GameController.GetPrefab<unit_Jailbreak>();
        squadSize = 1;
        eCost = 3;
    }
}
