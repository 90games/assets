﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnJesus : card_SpawnUnitGeneric
{
    public card_SpawnJesus()
    {
        cardName = "Spawn Jesus";
        prefab = GameController.GetPrefab<unit_Jesus10>();
        squadSize = 1;
        eCost = 4;
    }
}
