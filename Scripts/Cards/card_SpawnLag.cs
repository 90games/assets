﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnLag : card_SpawnUnitGeneric {

    public card_SpawnLag()
    {
        cardName = "Spawn Lag";
        prefab = GameController.GetPrefab<unit_Lag>();
        squadSize = 1;
        eCost = 3;
    }
}