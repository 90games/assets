﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnTroubleshooter : card_SpawnUnitGeneric {
    
    public card_SpawnTroubleshooter()
    {
        cardName = "Spawn Troubleshooter";
        prefab = GameController.GetPrefab<unit_Troubleshooter>();
        eCost = 4;
        squadSize = 1;
    }
}
