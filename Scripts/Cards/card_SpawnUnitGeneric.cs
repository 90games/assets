﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class card_SpawnUnitGeneric : Card
{

    public GameObject prefab;
    public int squadSize=1;

    public card_SpawnUnitGeneric()
    {
    }

    public override void Activate()
    {
        GameObject commander = AI.Spawn(spawnHere, goHere.GetComponent<NodeControl>(), prefab, owner.teamID);
        for (int i = 0; i < squadSize-1; i++)
        {
            //check if unit is squadable
            if(prefab.GetComponent<type_Squad>() == null)
            {
                Debug.LogError("Attempted to spawn a squad of unsquaddable units! ABORT");
                break;
            }
            GameObject subordinate = AI.Spawn(spawnHere, goHere.GetComponent<NodeControl>(), prefab, owner.teamID);
            commander.GetComponent<unit_Worm10>().squadMembers.Add(subordinate.GetComponent<type_Squad>());
            subordinate.GetComponent<type_Squad>().squadCommander = commander.GetComponent<type_Squad>();
        }
        base.Activate();
    }
}
