﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnWorm : card_SpawnUnitGeneric
{
    public card_SpawnWorm()
    {
        cardName = "Spawn Worm Squad";
        prefab = GameController.GetPrefab<unit_Worm10>();
        squadSize = 5;
        eCost = 6;
    }
}
