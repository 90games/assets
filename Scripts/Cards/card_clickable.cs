﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class card_clickable : MonoBehaviour {

    //Public
    public Card card;

    //Private
    private Color baseColor;
    public Text cardText;
    public LayerMask nodes;

    //Caching
    private SpriteRenderer sRenderer;


    // Use this for initialization
    void Start () {

        sRenderer = gameObject.GetComponent<SpriteRenderer>();
        baseColor = sRenderer.color;
        cardText = transform.FindChild("Canvas").FindChild("Card Text").GetComponent<Text>();
        if (cardText == null) Debug.LogError("Card Text for " + name + " not found. Is the attachment names properly?");
        cardText.text = card.cardName + Environment.NewLine + card.eCost.ToString();
        nodes = LayerMask.GetMask("Nodes");
    }

    // Update is called once per frame
    void Update () {
        if (card.isReady)
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (isMouseOnNode() != null)
                {
                    card.spawnHere = isMouseOnNode().transform.position;
                    GameController.lRenderer.SetVertexCount(2);
                    GameController.lRenderer.SetPosition(0, card.spawnHere);
                }
            }

            if (Input.GetMouseButton(0))
            {
                GameController.lRenderer.SetVertexCount(2);
                if (isMouseOnNode() != null)
                {
                    card.goHere = isMouseOnNode().gameObject;
                    GameController.lRenderer.SetPosition(1, card.goHere.transform.position);
                }
                else
                {
                    card.goHere = null;
                    GameController.lRenderer.SetPosition(1, GameController.mouseCurrent);
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                GameController.lRenderer.SetVertexCount(0);
                if (card.goHere != null)
                {
                    card.Activate();
                    Destroy(gameObject);
                }
                else card.spawnHere = Vector3.zero;
            }

            if (Input.GetMouseButtonDown(1))
            {
                card.isReady = false;
                sRenderer.color = baseColor;
                card.spawnHere = Vector3.zero;
                card.goHere = null;
            }
        }
    }

    public void OnMouseEnter()
    {
        transform.localScale *= 1.2f;
    }

    private void OnMouseExit()
    {
        transform.localScale = Vector3.one;
    }

    private void OnMouseDown()
    {
        if (card.isReady)
        {

        }

        if (card.owner.energy >= card.eCost && !card.isReady)
        {
            card.isReady = true;
            sRenderer.color = Color.green;
        }
        if (card.owner.energy < card.eCost && !card.isReady)
        {
            StartCoroutine(Deny());
        }

    }

    private NodeControl isMouseOnNode()
    {
        Collider2D[] cols = Physics2D.OverlapPointAll(GameController.mouseCurrent, nodes);
        foreach (Collider2D col in cols)
        {
            Debug.Log("Checking " + col.name);
            if (col.gameObject.GetComponent<NodeControl>() != null)
            {
                Debug.Log("Mouse is on node");
                return col.gameObject.GetComponent<NodeControl>();

            }
        }
        Debug.Log("Mouse is NOT on node!");
        return null;
    }

    private IEnumerator Deny()
    {
        sRenderer.color = Color.red;
        yield return new WaitForSeconds(1);
        sRenderer.color = baseColor;
    }
}
