﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_novajedinica : card_SpawnUnitGeneric
{

    public card_novajedinica()
    {
        cardName = "spawn novajedinica";
        prefab = GameController.GetPrefab<unit_Shooser>();
        squadSize = 1;
        eCost = 3;
    }
}
