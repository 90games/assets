﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class card_SpawnSpam : card_SpawnUnitGeneric {

    public card_SpawnSpam()
    {
        cardName = "Spawn Spam";
        prefab = GameController.GetPrefab<unit_Spam>();
        squadSize = 1;
        eCost = 3;
    }
}
