﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class explosion : MonoBehaviour {

    public float damage;
    public float radius;
    public int teamID;
    public bool friedlyFire;
    public AI originator;

    public Vector3 finalScale;

    //Caching
    private SpriteRenderer sRenderer;

	// Use this for initialization
	void Start () {
        sRenderer = GetComponent<SpriteRenderer>();
        finalScale.x = radius / sRenderer.sprite.bounds.extents.x;
        finalScale.y = radius / sRenderer.sprite.bounds.extents.y;
        finalScale.z = 1;
        transform.localScale = Vector3.zero;
        sRenderer.color = Color.red;
        StartCoroutine(Explode());
    }

    // Update is called once per frame
    void Update () {
	
	}

    private IEnumerator Explode()
    {
        for(float i=0; i<0.5f; i += Time.deltaTime)
        {
            transform.localScale = new Vector3(finalScale.x*i*2, finalScale.y*i*2, 0);
            Debug.Log("adding scale");
            yield return new WaitForEndOfFrame();
        }

        Debug.Log("Exploding");
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, radius, GameController.units);
        List<AI> targets = new List<AI>();
        foreach(Collider2D col in cols)
        {
            if(col.transform.parent.gameObject.GetComponent<AI>() != null)
            {
                AI ai = col.transform.parent.gameObject.GetComponent<AI>();
                if (friedlyFire) targets.Add(ai);
                else if (ai.teamID != teamID) targets.Add(ai);
            }

            foreach (AI unit in targets) unit.OnReceiveDamage(damage, originator);
        }

        Color baseColor = sRenderer.color;
        for(float i = 0; i<0.25; i += Time.deltaTime)
        {
            sRenderer.color = new Color(baseColor.r, baseColor.g, baseColor.b, 1 - 4 * i);
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
    }
}
