﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;


//TODO: organize variables here
//TODO: seriously, organize variables in here

public class GameController : MonoBehaviour {

    //teamLists keep track of all units on the map sorted into various teams
    //these are static so you can use them via GameController.teamLists[teamID][unitID]

    public static int _teams=2;
    public int __teams;

    /// <summary>
    /// Manually input the number of non-neutral teams in the scene (very important!!)
    /// </summary>
    public static int teams { get { return _teams; } }

    public static Vector2 mouseCurrent
    {
        get
        {
            Vector3 v = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            v.z = 0;
            return v;
        }
    }

    public static List<List<GameObject>> teamLists = new List<List<GameObject>>();
    public static List<int> ticketList = new List<int>();
    public static List<GameObject> bases = new List<GameObject>();
    public static List<GameObject> nodes = new List<GameObject>();
    public static List<Player> playerList;

    /// <summary>
    /// How much energy does each player obtain every tick?
    /// </summary>
    public int energyPerTick = 1;
    
    /// <summary>
    /// How much time (in seconds) passes between ticks?
    /// </summary>
    public int tickDelay = 1;

    /// <summary>
    /// What is the maximum amount of energy a player can have?
    /// </summary>
    public int eMax = 10;

    /// <summary>
    /// Maximum number of cards a player can hold at any given time
    /// </summary>
    public int maxCards = 5;

    public float cameraMoveSpeed = 100;
    private Camera cam;
    private Vector3 camTranslate;
    public List<GameObject> nodesPublic;
    public float FPS;
    public static List<deathLog> killBoard;
    public GameObject prefabTest;
    public GameObject spawnPointTest;
    public Text eDisplay;
    public GameObject cardTest;
    public static LineRenderer lRenderer;
    public Image restockTimerImage;
    public float restockTimer=0;
    public Text activeUnitInfo;
    public static AI activeUnit;

    public float worldUnitsPerScreenPixel { get { return (Camera.main.orthographicSize * 2f) / Camera.main.pixelHeight; } }
    public float cameraWidth { get { return worldUnitsPerScreenPixel * Camera.main.pixelWidth; } }
    public float cameraHeight { get { return worldUnitsPerScreenPixel * Camera.main.pixelHeight; } }

    //Movement:
    //public static pos_grid mainPosGrid;

    //Pathfinding
    private GridGraph graph;
    public static GridGraph mainGraph;
    public static AstarPath aPath;

    //Caching:
    public static GameController main;
    public static LayerMask units;
    private GameObject mainCanvas;
    private RectTransform mainCanvasRect;
    public GameObject _controlRectPFB;
    public static GameObject controlRectPFB;

    //Temporary:
    public List<GameObject> _allCardPrefabs = new List<GameObject>();
    public static List<GameObject> allCardPrefabs = new List<GameObject>();
    public string activeUnitname;

    //Debugging:
    public GameObject lineDisplayHolder;
    private LineRenderer lineDisplay;
    public GameObject lineVertex1;
    public GameObject lineVertex2;
    public bool healthInfo = true;
    public int unitCount = 0;

    void Awake()
    {
        playerList = new List<Player>();
        allCardPrefabs = _allCardPrefabs;
        units = LayerMask.GetMask("Units");
        controlRectPFB = _controlRectPFB;
    }

    // Use this for initialization
    void Start () {
        main = this;
        int j = 0;
        for(int i=0; i< teams; i++)
        {
            string teamName = "Team" + i;
            GameObject[] temp = GameObject.FindGameObjectsWithTag(teamName);
            List<GameObject> temp2 = new List<GameObject>();
            foreach(GameObject item in temp)
            {
                temp2.Add(item);
            }
            teamLists.Add(temp2);
            ticketList.Add(j);
        }
        teamLists.Add(new List<GameObject>());
        GameObject[] units = GameObject.FindGameObjectsWithTag("Unit");
        foreach(GameObject unit in units)
        {
            teamLists[teamLists.Count - 1].Add(unit);
        }
        GameObject[] temp1 = GameObject.FindGameObjectsWithTag("Node");
        if (temp1.Length == 0) Debug.LogError("No nodes found. ABORT! ABORT!");
        foreach (GameObject item in temp1)
        {
            nodes.Add(item);
        }
        killBoard = new List<deathLog>();
        cam = Camera.main;
        camTranslate = new Vector3();
        StartCoroutine(Tick());
        Physics2D.queriesHitTriggers = true;
        lRenderer = GetComponent<LineRenderer>();
        StartCoroutine(LateStart());
        StartCoroutine(CardStocker());
        lineDisplay = lineDisplayHolder.GetComponent<LineRenderer>();
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        mainCanvasRect = mainCanvas.GetComponent<RectTransform>();
        aPath = GameObject.FindGameObjectWithTag("Astar Path").GetComponent<AstarPath>();
        graph = (GridGraph)aPath.graphs[0];
        mainGraph = graph;

    }


    private IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();
        energyPerTick = Settings.energyPerTick;
        tickDelay = Settings.tickRate;

    }

    // Update is called once per frame
    void Update () {
        __teams = playerList.Count;
        FPS = 1 / Time.deltaTime;
        foreach (List<GameObject> item in teamLists) FilterList(item);
        nodesPublic = nodes;
        camTranslate.x = Input.GetAxis("Horizontal");
        camTranslate.y = Input.GetAxis("Vertical");
        cam.transform.Translate(camTranslate * cameraMoveSpeed * Time.deltaTime);
        eDisplay.text = "";
        for (int i=0; i<playerList.Count; i++)
        {
            eDisplay.text += "Player " + i + " energy: " + playerList[i].energy + Environment.NewLine;
        }
        //sort playerList:
        for(int i=0; i<playerList.Count; i++)
        {
            int teamIDtemp = playerList[i].teamID;
            Player temp = playerList[teamIDtemp];
            playerList[teamIDtemp] = playerList[i];
            playerList[i] = temp;

        }
        restockTimer += Time.deltaTime;
        restockTimerImage.fillAmount = restockTimer / Settings.cardRefreshRate;

        if (Input.GetAxis("Mouse ScrollWheel") < 0) cam.orthographicSize *= 1.1f;
        if (Input.GetAxis("Mouse ScrollWheel") > 0) cam.orthographicSize /= 1.1f;
        if (Input.GetButtonDown("ShowDebugger"))
        {
            healthInfo = !healthInfo;
            ToggleHealthInfo();
        }

        //aPath.Scan();

        unitCount = GetUnitCount();
        Debugger();
    }

    private void ToggleHealthInfo()
    {
        foreach(List<GameObject> list in teamLists)
        {
            foreach(GameObject unit in list)
            {
                unit.GetComponent<AI>().infoText.gameObject.SetActive(healthInfo);
            }
        }
    }

    private int GetUnitCount()
    {
        int i = 0;
        foreach (List<GameObject> list in teamLists) i += list.Count;
        return i;
    }

    private void Debugger()
    {
        if (activeUnit != null)
        {
            activeUnitname = activeUnit.name;
            activeUnitInfo.text = "name: " + activeUnit.name +
                                    "\ntype: " + activeUnit.type +
                                    "\nteam: " + activeUnit.teamID +
                                    "\nhp: " + activeUnit.hp + "/" + activeUnit.maxhp +
                                    "\nduration: " + Mathf.Round(activeUnit.lifeTime) + "/" + activeUnit.maxLifeTime +
                                    "\ntarget: " + (activeUnit.target == null ? "None" : activeUnit.target.name);
            lineDisplay.SetVertexCount(3);
            lineDisplay.SetPosition(0, CanvasToWorldPoint(lineVertex1.GetComponent<RectTransform>()));
            lineDisplay.SetPosition(1, CanvasToWorldPoint(lineVertex2.GetComponent<RectTransform>()));
            lineDisplay.SetPosition(2, activeUnit.transform.position);
        }
        else {
            activeUnitInfo.text = "No active unit selected";
            lineDisplay.SetVertexCount(0);
        }
    }

    private IEnumerator Tick()
    {
        while (true)
        {
            yield return new WaitForSeconds(tickDelay);
            for (int i = 0; i < playerList.Count; i++)
            {
                Debug.Log("Adding energy to team " + i);
                if (playerList[i].energy + energyPerTick <= Settings.maximumEnergy) playerList[i].energy += energyPerTick;
                else playerList[i].energy = Settings.maximumEnergy;
            }
        }
    }

    private IEnumerator CardStocker()
    {
        while (true)
        {
            restockTimer = 0;
            yield return new WaitForSeconds(Settings.cardRefreshRate);
            Debug.Log("<color=red>Restocking now</color>");
            foreach(Player player in playerList)
            {
                player.restockOneCard();
            }
        }
    }

    public static void GridScan()
    {
        aPath.Scan();
    }

    public static GameObject GetPrefab<T>(){
        foreach(GameObject obj in allCardPrefabs)
        {
            if (obj.GetComponent<T>() != null) return obj;
        }
        return null;
    }

    /// <summary>
    /// Filters and converts an array of Collider2D's into a List[AI] (units only!)
    /// </summary>
    /// <param name="cols"></param>
    /// <returns></returns>
    public static List<AI> ColsArrayToList(Collider2D[] cols)
    {
        List<AI> AIList = new List<AI>();
        foreach(Collider2D col in cols)
        {
            if(col.transform.parent.gameObject.GetComponent<AI>()!= null)
            {
                AIList.Add(col.transform.parent.gameObject.GetComponent<AI>());
            }
        }
        return AIList;
    }

    public static void FilterList<T>(List<T> list)
    {
        if (list.Count == 0) return;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i] == null) list.RemoveAt(i);
        }
    }

    public void TempEnableAggro()
    {
        foreach (GameObject unit in teamLists[0]) unit.GetComponent<AI>().selectByAggro = true;
    }

    public void TempDisableAggro()
    {
        foreach (GameObject unit in teamLists[0]) unit.GetComponent<AI>().selectByAggro = false;
    }

    public void WriteDeathLog()
    {
        foreach (deathLog log in killBoard)
        {
            Debug.Log("name: " + log.name);
            Debug.Log("killed by: " + log.killedBy);
            Debug.Log("time of death: " + log.timeOfDeath);
            Debug.Log("assists: " + log.assists.ToString());
        }
    }

    private Vector3 CanvasToWorldPoint(RectTransform rtransform)
    {
        float x = rtransform.position.x / mainCanvasRect.rect.width;
        float y = rtransform.position.y / mainCanvasRect.rect.height;
        Vector3 v = Camera.main.ViewportToWorldPoint(new Vector3(x, y, 0));
        v.z = -10;

        return v;
    }

    private void ApplyBuffTest()
    {
        Debug.Log("hello");
        GameObject someUnit = GameObject.FindGameObjectWithTag("Team0");
        AI someAI = someUnit.GetComponent<AI>();
        Debug.Log("Applying Lag to " + someUnit.name);
        someUnit.AddComponent<effect_debuff_Lag>();
    }

    public void SpawnUnitTest()
    {
        AI.Spawn(spawnPointTest.transform.position, 1, 1, 4, 10, 1, 100, 0, 0, 100, new Vector3(1, 1, 1), prefabTest);
    }

    
}
