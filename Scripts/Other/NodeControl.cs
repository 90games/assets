﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class NodeControl : MonoBehaviour {

    public float capRadius = 5;
    public int ownerTeamID = -1;
    private int partialOwnerTeamID = -1;
    public int cappingTeamID = -1;
    public float _ownership = 0;
    public float ownership { get { return Mathf.Clamp(_ownership, 0, 100); } set { _ownership = value; } }
    public int ticketValue = 10;
    public Text ownerDisp;
    public Text ownershipDisp;
    public List<GameObject> linkedNodes;
    public List<AI> unitsPresent;
    public LayerMask units;
    public List<float> totalCapPowers;
    public List<float> ownershipList;


    // Use this for initialization
    void Start () {
        units = LayerMask.GetMask("Units");
        totalCapPowers = new List<float>(2);
        assignColor();
        foreach(GameObject node in linkedNodes)
        {
            if (!node.GetComponent<NodeControl>().linkedNodes.Contains(gameObject)) node.GetComponent<NodeControl>().linkedNodes.Add(gameObject);
        }
        InvokeRepeating("determineCap", 1, 0.1f);
        Invoke("InitializeOwneshipList", 0);
	}
	
	// Update is called once per frame
	void Update () {
        ownerDisp.text = ownerTeamID.ToString();
        ownershipDisp.text = Mathf.RoundToInt(ownership).ToString();
        
        Debugger();
	}

    void LateUpdate()
    {
        if (ownerTeamID != -1 && ownership <= 0)
        {
            ownerTeamID = -1;
            assignColor();
        }
        if(ownerTeamID==-1 && ownership>=100)
        {

            ownerTeamID = partialOwnerTeamID;
            assignColor();
        }
    }
    
    private void Debugger()
    {
        if (linkedNodes.Count != 0)
        {
            foreach (GameObject node in linkedNodes)
            {
                Debug.DrawLine(transform.position, node.transform.position);
            }
        }
    }

    private void InitializeOwnershipList()
    {
        Debug.Log("Initializing ownershipList");
        ownershipList = new List<float>(GameController.teams);
        for (int i=0; i<GameController.teams; i++)
        {
            ownershipList.Add(0);
        }
    }

    private bool CheckForCleanOwnershipList()
    {
        int ownerCount = 0;
        for(int i=0; i<ownershipList.Count; i++)
        {
            ownershipList[i] = Mathf.Clamp(ownershipList[i], 0, 100);
            if (ownershipList[i] > 0)
            {
                ownerCount++;
                partialOwnerTeamID = i;
                ownershipDisp.text = ownershipList[i].ToString();
            }
        }
        if (ownerCount == 0) { partialOwnerTeamID = -1; return true; }
        else if (ownerCount > 1)
        {
            ownershipDisp.text = "ERROR";
            return false;
        }
        else return true;
    }

    private void determineCap()
    {
        Debug.Log("determineCap() called");
        //Detect units present in the cap zone:
        unitsPresent.Clear();
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, capRadius, units);
        foreach(Collider2D col in cols)
        {
            unitsPresent.Add(col.transform.parent.gameObject.GetComponent<AI>());
        }

        totalCapPowers.Clear();
        for(int i=0; i<GameController.teams; i++) totalCapPowers.Add(0);
        foreach(AI unit in unitsPresent)
        {
            totalCapPowers[unit.teamID] += unit.capSpeed;
        }

        //Determine team with the most totalCapPowers
        float maxPower = 0;
        int maxIndex = -1;
        for(int i=0; i<totalCapPowers.Count; i++)
        {
            if(totalCapPowers[i]>maxPower)
            {
                maxPower = totalCapPowers[i];
                maxIndex = i;
            }
        }
            
        //Determine if they outweigh the combined totalPower of all other teams present
        float maxOpposingPower = 0;
        for(int i=0; i<totalCapPowers.Count; i++)
        {
            if (i == maxIndex) continue;
            else maxOpposingPower += totalCapPowers[i];
        }

        //Proceed with capture 
        if (ownership == 0 && maxPower> maxOpposingPower)
        {
            partialOwnerTeamID = maxIndex;
            ownership += (maxPower - maxOpposingPower)/10;
        }
        if (ownership > 0)
        {
            float ownerPower = totalCapPowers[partialOwnerTeamID];
            float guestPower = 0;
            for(int i=0; i<totalCapPowers.Count; i++)
            {
                if (i == partialOwnerTeamID) continue;
                guestPower += totalCapPowers[i];
            }
            ownership += (ownerPower - guestPower)/10;
        }
        

        //else
        //{
        //    //Determine totalCapPower for defenders and aggressors
        //    float defenderPower = 0;
        //    float aggressorPower = 0;
        //    foreach(AI unit in unitsPresent)
        //    {
        //        if (unit.teamID == ownerTeamID) defenderPower += unit.capSpeed;
        //        else aggressorPower += unit.capSpeed;
        //    }

        //    //Proceed with capture
        //    ownership -= (aggressorPower - defenderPower) / 10;
        //    if (aggressorPower > defenderPower) cappingTeamID = -1;
        //    else cappingTeamID = ownerTeamID;
        //}
    }
    
    /// <summary>
    /// Assign color according to which team owns the node - grey if neutral
    /// </summary>
    /// <returns></returns>
    private void assignColor()
    {
        switch (ownerTeamID)
        {
            case 0:
                GetComponent<SpriteRenderer>().color = Color.green;
                break;
            case 1:
                GetComponent<SpriteRenderer>().color = Color.red;
                break;
            case 2:
                GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
            case 3:
                GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            default:
                GetComponent<SpriteRenderer>().color = Color.grey;
                break;
        }
    }

}
