﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Player : MonoBehaviour {

    //Public
    public int teamID;
    public int energy;
    public int energyMax=10;
    public Deck deck;
    public float cardRespawnDelay=2;
    public int maxCards=5;
    public List<Card> hand;
    public List<Card> discardPile;
    public NodeControl baseNode;

    //Debugging
    public int handCount;

    //Private
    public List<bool> positionsRespawning;

	// Use this for initialization
	protected void Start () {
        hand = new List<Card>();
        deck = new Deck();
        positionsRespawning = new List<bool>();
        GameController.playerList.Add(this);
        StartCoroutine(LateStart());
    }

    private IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < maxCards; i++)
        {
            hand.Add(deck.draw());
            hand[i].owner = this;
            Debug.Log("added "+ hand[i].cardName + " to hand");
            positionsRespawning.Add(false);
        }
        energyMax = Settings.maximumEnergy;
        energy = energyMax;
        cardRespawnDelay = Settings.cardRefreshRate;
        maxCards = Settings.maxCardsInHand;
    }

    // Update is called once per frame
    protected void Update () {
        //for(int i=0; i<maxCards; i++)
        //{
        //    if (hand[i] != null)
        //    {
        //        if (hand[i].used == true)
        //        {
        //            StartCoroutine(respawnCardAtIndex(i));
        //            hand[i] = null;
        //        }
        //    }
        //}
        handCount = hand.Count;
        cleanup();
    }

    protected virtual IEnumerator respawnCardAtIndex(int index)
    {
        if (!positionsRespawning[index])
        {
            Debug.Log("POSITION 2: commencing respawn of position " + index);
            positionsRespawning[index] = true;
            yield return new WaitForSeconds(cardRespawnDelay);
            Debug.Log("POSITION 3: pause processed");
            hand[index] = deck.draw();
            hand[index].owner = this;
            attachToObject(hand[index], index);
            positionsRespawning[index] = false;
        }
    }

    protected virtual void restockCardAtIndex(int index)
    {
        Debug.Log("<color=red>Restocking card at position </color>" + index + "<color=red> for </color>" + name);
        hand[index] = deck.draw();
        hand[index].owner = this;
        attachToObject(hand[index], index);
        positionsRespawning[index] = false;
    }

    public void restockAllCards()
    {
        Debug.Log("<color=red>restockAllCards() called for </color>" + name);
        for (int i = 0; i < Settings.maxCardsInHand; i++)
        {
            if (hand[i] != null)
            {
                if (hand[i].used == true)
                {
                    Debug.Log("<color=red>calling restock</color>");
                    restockCardAtIndex(i);
                }
            }
        }
    }

    public void restockOneCard()
    {
        for (int i = 0; i < Settings.maxCardsInHand; i++)
        {
            if (hand[i] != null)
            {
                if (hand[i].used == true)
                {
                    Debug.Log("<color=red>calling restock</color>");
                    restockCardAtIndex(i);
                    return;
                }
            }
        }
    }

    //move down to PlayerController later

    protected virtual void attachToObject(Card card, int i) { }

    protected virtual void cleanup()
    {
        for(int i=maxCards; i< hand.Count; i++)
        {
            hand[i] = null;
        }
    }
}
