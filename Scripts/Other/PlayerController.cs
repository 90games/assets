﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : Player
{

    //Public
    public GameObject blankCardPrefab;

    //Private
    private card_clickable activeCard;
    public List<card_clickable> realCards;
    private Camera camera;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        Debug.Log("Helloooo");
        realCards = new List<card_clickable>();
        StartCoroutine(LateStart());
        maxCards = Settings.maxCardsInHand;
    }

    private IEnumerator LateStart()
    {
        yield return new WaitForEndOfFrame();
        for (int i = 0; i < maxCards; i++)
        {
            realCards.Add(Instantiate(blankCardPrefab).GetComponent<card_clickable>());
            Debug.Log("Setting card component of " + realCards[i].name + " to " + hand[i].cardName);
            realCards[i].GetComponent<card_clickable>().card = hand[i];
            Debug.Log("hello");
        }
        camera = Camera.main;
        cleanup();
    }

        // Update is called once per frame
        new void Update()
    {
        base.Update();
        Debug.Log(hand.Count);
        Debug.Log(realCards.Count);
        for (int i = 0; i < maxCards; i++)
        {
            if (hand[i] == null) Debug.Log("there is no card at index " + i);
            else if (realCards[i] != null)
            {
                realCards[i].transform.position = cardPositionByIndex(i);

            }
        }
    }


    //outdated
    protected override IEnumerator respawnCardAtIndex(int index)
    {
        if (positionsRespawning[index] == false)
        {
            Debug.Log("POSITION 1: Commencing lower level respawn coroutine");
            Debug.Log("POSITION 2: commencing respawn of position " + index);
            positionsRespawning[index] = true;
            yield return new WaitForSeconds(cardRespawnDelay);
            Debug.Log("POSITION 3: pause processed");
            hand[index] = deck.draw();
            hand[index].owner = this;
            hand[index].teamID = this.teamID;
            attachToObject(hand[index], index);
            positionsRespawning[index] = false;

            Debug.Log("POSITION 4: Base coroutine processed");
            if (hand[index] == null)
            {
                Debug.LogError("respawn mechanic failure");
            }
        }
    }

    protected override void restockCardAtIndex(int index)
    {
        Debug.Log("<color=red>Restocking card at position </color>" + index + "<color=red> for </color>" + name);
        hand[index] = deck.draw();
        hand[index].owner = this;
        hand[index].teamID = this.teamID;
        attachToObject(hand[index], index);
        positionsRespawning[index] = false;
    }

    protected Vector3 cardPositionByIndex(int index)
    {
        Vector3 leftPoint = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth / 3, camera.pixelHeight / 10, 0));
        Vector3 rightPoint = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth * 2 / 3, camera.pixelHeight / 10, 0));
        float y;
        switch (teamID)
        {
            case 0:
                y = leftPoint.y;
                break;
            case 1:
                y = camera.ScreenToWorldPoint(new Vector3(camera.pixelWidth / 3, Screen.height - camera.pixelHeight / 10, 0)).y;
                break;
            default:
                y = 1000;
                break;
        }

        float newX = leftPoint.x + index * (rightPoint.x - leftPoint.x) / maxCards;
        return new Vector3(newX, y, 0);
    }

    protected override void attachToObject(Card card, int i)
    {
        Debug.Log("attaching");
        card_clickable newCard = Instantiate(blankCardPrefab).GetComponent<card_clickable>();
        realCards[i]=newCard;
        newCard.card = card;
        newCard.transform.position = cardPositionByIndex(i);
    }

    protected override void cleanup()
    {
        base.cleanup();
        for (int i = maxCards; i < realCards.Count; i++)
        {
            Destroy(realCards[i]);
        }
    }

}