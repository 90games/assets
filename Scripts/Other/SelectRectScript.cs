﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// s=selected

public class SelectRectScript : MonoBehaviour {

    private Vector3 mPos1;
    private Vector3 mPos2;

    public List<AI> sUnits;
    public List<GameObject> sGFX;
    public GameObject sGFX_PFB;

    public bool sActive;

    //Caching
    private SpriteRenderer sRenderer;
    private float rectX, rectY;

	// Use this for initialization
	void Start () {
        sRenderer = GetComponent<SpriteRenderer>();
        rectX = sRenderer.bounds.size.x;
        rectY = sRenderer.bounds.size.y;

        transform.localScale = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {

        FilterLists();
        UpdateRect();
        UpdateGFX();

        if (sActive)
        {
            GrabUnits();

            if (Input.GetMouseButton(0))
            {
                mPos2 = GameController.mouseCurrent;
            }

            if (Input.GetMouseButtonUp(0))
            {
                sActive = false;
                transform.localScale = Vector3.zero;
                mPos2 = mPos1;
            }
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
            {
                mPos1 = GameController.mouseCurrent;
                mPos2 = mPos1;
                sUnits.Clear();
                sActive = true;
            }

            if (Input.GetMouseButtonDown(1))
            {
                NodeControl node = AI.NearestNode(GameController.mouseCurrent);
                Debug.Log("Attempging to assign " + node.name + " as target for units");
                foreach(AI unit in sUnits)
                {
                    unit.targetNode = node.gameObject;
                }
            }
        }
	}

    /// <summary>
    /// Updates the Rect position according to initial and current mouse positions
    /// </summary>
    private void UpdateRect()
    {
        transform.position = (mPos1 + mPos2) / 2;
        float x = (mPos2.x - mPos1.x) / rectX;
        float y = (mPos2.y - mPos1.y) / rectY;
        transform.localScale = new Vector3(x, y, 0);
    }

    /// <summary>
    /// Updates the number and positions of selection indicators
    /// </summary>
    private void UpdateGFX()
    {
        while (sUnits.Count > sGFX.Count)
        {
            sGFX.Add(Instantiate(sGFX_PFB));
        }

        while (sUnits.Count < sGFX.Count)
        {
            Destroy(sGFX[sGFX.Count - 1]);
            sGFX.RemoveAt(sGFX.Count - 1);
        }

        for(int i=0; i<sUnits.Count; i++)
        {
            sGFX[i].transform.position = sUnits[i].transform.position;
        }
    }

    /// <summary>
    /// Updates the selected unit list via Physics2D.OverlapAreaAll
    /// </summary>
    private void GrabUnits()
    {
        Collider2D[] cols = Physics2D.OverlapAreaAll(mPos1, mPos2, GameController.units);
        sUnits = GameController.ColsArrayToList(cols);
    }

    private void FilterLists()
    {
        for(int i=0; i<sUnits.Count; i++)
        {
            if (sUnits[i] == null) sUnits.RemoveAt(i);
        }

        for(int i=0; i<sGFX.Count; i++)
        {
            if (sGFX[i] == null) sGFX.RemoveAt(i);
        }
    }
}
