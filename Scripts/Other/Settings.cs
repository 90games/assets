﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Settings : MonoBehaviour {

    private static Settings _main;
    public static Settings main { get { return _main; } }

    [Tooltip("The amount of energy each player gets per tick")]
    /// <summary>
    /// The amount of energy each player gets per tick
    /// </summary>
    public int _energyPerTick;
    public static int energyPerTick;

    [Tooltip("The time span (in seconds) between 2 ticks")]
    /// <summary>
    /// The time span (in seconds) between 2 ticks
    /// </summary>
    public int _tickRate;
    public static int tickRate;

    [Tooltip("Maximum amount of energy a player can have")]
    /// <summary>
    /// Maximum amount of energy a player can have
    /// </summary>
    public int _maximumEnergy;
    public static int maximumEnergy;

    [Tooltip("The time (in seconds) it takes for a used card to refresh")]
    /// <summary>
    /// The time (in seconds) it takes for a used card to refresh
    /// </summary>
    public int _cardRefreshRate = 1;
    public static int cardRefreshRate;

    [Tooltip("Maximum number of cards a player can hold")]
    /// <summary>
    /// Maximum number of cards a player can hold
    /// </summary>
    public int _maxCardsInHand = 1;
    public static int maxCardsInHand;

    [Tooltip("How much energy each card costs")]
    public Dictionary<string, int> cardECosts;

    [Tooltip("Can units be micro controlled?")]
    public bool _microControl = false;
    public static bool microControl;

    Camera cam = Camera.main;
    
    void Awake()
    {
        energyPerTick = _energyPerTick;
        tickRate = _tickRate;
        maximumEnergy = _maximumEnergy;
        cardRefreshRate = _cardRefreshRate;
        maxCardsInHand = _maxCardsInHand;
        microControl = _microControl;
    }

    void Start()
    {
        if (_main != null)
        {
            Debug.LogError("Multiple Settings classes detected - fixing");
            Destroy(this);
        }
        else _main = this;

    }
}
