﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public class pos_grid{

    public List<pos_grid_node> gridList;
    public static pos_grid main;
    public float nodeSize;
    public float width { get { return nodeSize * gridList.Count; } }
    public float height { get { return nodeSize * gridList.Count; } }
    public int int_Width;

    //Caching
    private AstarPath aPath;
    private GridGraph mainGraph;

    public pos_grid()
    {
        if (main != null) main = this;
        aPath = GameObject.FindGameObjectWithTag("Astar Path").GetComponent<AstarPath>();
        mainGraph = (GridGraph)aPath.graphs[0];
        this.nodeSize = mainGraph.nodeSize;
        this.int_Width = mainGraph.width;
        gridList = new List<pos_grid_node>();
        generateGrid();
    }

    private void generateGrid()
    {
        int i = 0;
        foreach(GridNode gridNode in mainGraph.nodes)
        {
            pos_grid_node newNode = new pos_grid_node(gridNode, this);
            newNode.gridPositionInList = gridList.Count;
            gridList.Add(newNode);
            Debug.Log("adding node #" + i++);
        }
        Debug.Log("node 1 position ###" + gridList[0].position);
    }

    public pos_grid_node NearestNode(Vector3 position)
    {
        float minSqrDist = (gridList[0].position - position).sqrMagnitude;
        pos_grid_node nearestNode = gridList[0];
        foreach(pos_grid_node node in gridList)
        {
            if ((node.position - position).sqrMagnitude < minSqrDist)
            {
                minSqrDist = (node.position - position).sqrMagnitude;
                nearestNode = node;
            }
        }
        
        return nearestNode;
    }

    public pos_grid_node getNode(int x, int y)
    {
        return gridList[x + y];
    }
}
