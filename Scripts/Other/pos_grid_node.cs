﻿using UnityEngine;
using System.Collections;
using Pathfinding;

public class pos_grid_node{
    private Vector3 _position;
    public Vector3 position { get { return _position; } }
    public bool isOccupied;
    public GameObject occupant;
    public int gridPositionInList;
    public int gridPositionX { get { return boundNode.NodeInGridIndex % GridNode.GetGridGraph(boundNode.GraphIndex).width; } }
    public int gridPositionY { get { return boundNode.NodeInGridIndex % GridNode.GetGridGraph(boundNode.GraphIndex).width; } } //are these really necessary?
    public GridNode boundNode;
    public pos_grid grid;
    public float nodeSize { get { return grid.nodeSize; } }

    public pos_grid_node(GridNode node, pos_grid grid)
    {
        this.grid = grid;
        Debug.Log("check 2");
        grid.gridList[gridPositionInList] = this;
        Debug.Log("check 2.5");
        _position = (Vector3)node.position;
        Debug.Log("check 3");
        boundNode = node;
    }

    public bool checkIfOccupied()
    {
        Collider2D[] cols = Physics2D.OverlapCircleAll(position, grid.nodeSize);
        foreach(Collider2D col in cols)
        {
            if (col.tag != "Node") return true;
        }
        return false;
    }

    public bool isAdjacent(Vector3 position)
    {
        float sqrDifference = (position - this.position).sqrMagnitude;
        if (sqrDifference < 8 * Mathf.Pow(nodeSize, 2)) return true;
        return false;
    }

    public bool isAdjacent(pos_grid_node otherNode)
    {
        if (this.grid != otherNode.grid) return false;
        if (Mathf.Abs(this.gridPositionX - otherNode.gridPositionX) <= 1 && Mathf.Abs(this.gridPositionY - otherNode.gridPositionY) <= 1) return true;
        return false;
    }
}
