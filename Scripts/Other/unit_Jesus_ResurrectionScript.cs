﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class unit_Jesus_ResurrectionScript : MonoBehaviour {

    public float resurrectionDelay;
    public Vector3 spawnPosition;
    public NodeControl targetNode;
    public int teamID;

	// Use this for initialization
	void Start () {
        StartCoroutine(resurrection());
	}

    IEnumerator resurrection()
    {
        Debug.Log("commence resurrection!");
        yield return new WaitForSeconds(resurrectionDelay);
        //GameObject newJesus = Spawn(this.transform.position, this.moveSpeed, this.damage, this.attackRange, this.acquisitionRange, this.RoF, this.maxhp, this.detectionRange, this.teamID, maxLifeTime, this.destination);
        GameObject newJesus = AI.Spawn(spawnPosition, targetNode, GameController.GetPrefab<unit_Jesus10>(), this.teamID);
        unit_Jesus10 tempJesusAI = newJesus.GetComponent<unit_Jesus10>();
        tempJesusAI.hasDied = true;
        Destroy(this);
    }
}
