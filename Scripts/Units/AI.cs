﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

//TODO: convert all List<GameObject> to List<AI> everywhere

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Seeker))]
[RequireComponent(typeof(LineRenderer))]

public abstract class AI : MonoBehaviour {

    public string type;
    public string behavior;
    public int teamID;

    //These are the core unit stats
    //statMP means stat multiplier - to be used for modifying stats in game (buffs, debuffs, etc.)
    //stat is now is stat*statMP - this should always be used when interacting with stats, never statBases or _statMPs
    //baseStats are legacy code and should be removed
    //stats will assume the role of baseStats - convert them to const at the end of project?
    //TODO: write summary for all stats

    [Tooltip("The unit's starting movespeed")]
    public float moveSpeedBase; 
    public float _moveSpeedMP; //backing field ALWAYS PRIVATE IN THE FUTURE - do not access in code! (use movespeedMP and moveSpeed instad)
    public float moveSpeedMP { get { return Mathf.Clamp(_moveSpeedMP, 0, Mathf.Infinity); } set { _moveSpeedMP = value; } }
    public float moveSpeed { get { return moveSpeedBase * moveSpeedMP; } }

    [Tooltip("The unit's starting damage")]
    public float damageBase = 5;
    public float _damageMP;
    public float damageMP { get { return Mathf.Clamp(_damageMP, 0, 1000); } set { _damageMP = value; } }
    public float damage { get { return damageBase * damageMP; } }

    [Tooltip("The unit's starting attack range")]
    public float attackRangeBase = 4;
    public float _attackRangeMP;
    public float attackRangeMP { get { return Mathf.Clamp(_attackRangeMP, 0, 1000); } set { _attackRangeMP = value; } }
    public float attackRange { get { return attackRangeBase * attackRangeMP; } }

    [Tooltip("The unit's starting vision range")]
    public float acquisitionRangeBase = 10;
    public float _acquisitionRangeMP;
    public float acquisitionRangeMP { get { return Mathf.Clamp(_acquisitionRangeMP, 0, Mathf.Infinity); } set { _acquisitionRangeMP = value; } }
    public float acquisitionRange { get { return acquisitionRangeBase * acquisitionRangeMP; } }

    [Tooltip("The unit's starting rate of fire")]
    public float RoFBase = 1; 
    public float _RoFMP;
    public float RoFMP { get { return Mathf.Clamp(_RoFMP, 0, Mathf.Infinity); } set { _RoFMP = value; } }
    public float RoF { get { return RoFBase * RoFMP; } }

    [Tooltip("The unit's starting hp")]
    public float _maxhp; //backing field for maxhp property - use this naming convention for future backing fields
    public float maxhp
    {
        get
        {
            if (_maxhp <= 0)
            {
                Debug.Log("Error - illegal maxhp value on " + name);
                return 1;
            }
            else return _maxhp;
        }
        set //when maxhp is altered, should hp be altered additively, multiplicatively or not at all?
        {
            hp *= (value / _maxhp);
            _maxhp = value;
        }
    }
    public float hp;
    public float hpPercentage { get { return hp / maxhp; } }

    [Tooltip("How long it takes for the unit to complete an attack, in seconds")]
    public float attackAnimationDuration = 0.1f;

    [Tooltip("The unit's starting duration, in seconds")]
    public float _maxLifeTime;
    public float maxLifeTime                    //Starting duration of the unit
    {
        get
        {
            if (_maxLifeTime <= 0)
            {
                Debug.Log("Error - illegal maxLifeTime value on " + name);
                return 1;
            }
            else return _maxLifeTime;
        }
        set //when maxLifeTime is altered, should lifeTime be altered additively, multiplicatively or not at all?
        {
            lifeTime *= (value / _maxLifeTime);
            _maxLifeTime = value;
        }
    }
    [Tooltip("The unit's current remaining duration, in seconds")]
    public float lifeTime;                      //How much duration (life time) is left

    public GameObject healthBar;                //The child object containing the healthbar
    public GameObject bodyGraphics;             //The child object that holds its visual appearance and rigidbody colliders
    public Text infoText;
    [Tooltip("Unit's threat level towards hostiles (more aggro means more likely to be targeted)")]
    public float aggro = 0;                     //Aggro - more aggro means more likely to be targeted by "intelligent" hostiles
    [Tooltip("Does the unit use aggro-based target selection?")]
    public bool selectByAggro = false;          //Does the unit determine target according to aggro?
    public bool stealthed = false;              //Is the unit stealthed? (overriden by detection)
    [Tooltip("Range in which the unit detects stealthed enemies - currently not fully implemented")]
    public float detectionRange = 0;            //Range in which stealthed units are revealed
    public GameObject target;                   //Attackers will attempt to engage target
    public bool immune = false;                 //Godmode (testing only (or is it?))
    [Tooltip("If the unit should naturally generate more aggro (casters, damage dealers) increase this multiplied")]
    public float aggroMultiplier = 1;          //Use this to designate high-value targets such as casters, buffers etc.
    private float healthBarScale;                //This operated the healthbar - less health means smaller healthBarScale
    public Vector3 destination = Vector3.zero;  //This is an alternative to defaultDirection - a point in the game space the unit should move towards
    public GraphNode destinationNode;

    [Tooltip("The percentage of a node that this unit will capture by itself per second")]
    /// <summary>
    /// The percentage of a node that this unit will capture by itself per second
    /// </summary>
    public float capSpeed = 1;

    /// <summary>
    /// What node is the unit currently located on? (null if none)
    /// </summary>
    public GameObject currentNode;
    
    private NodeControl currentNodeNodeControl;
    public GameObject targetNode;
    public GameObject finalTargetNode;
    public List<string> modifiers;
    public bool isCapping;
    public bool isDefensive;
    public List<AI> assistTracker;              //Tracks all units that assisted in the death of this unit
    public List<AI> killTracker;                //Tracks all units this one has killed
    public bool attackOnCoolDown;
    public List<misc_Effect> activeEffects;

    public List<GameObject> globalTargets = new List<GameObject>();         //These are lists used to sort all enemy units according to whether
    public List<GameObject> localTargets = new List<GameObject>();          //they are inside attackRange, acquisitionRange or outside both
    public List<GameObject> targetsInRange = new List<GameObject>();

    //Pathfinding
    public Path path;
    public int currentWaypoint = 0;
    public float nextWaypointDistance = 3;
    public bool pathIsEnded = false;
    public float updateDelay = 1;
    protected GridGraph mainGraph;

    //Caching
    protected Rigidbody2D rbody;
    protected Seeker seeker;
    protected LineRenderer lRenderer;

    //Debugging
    public bool showDebugger = true;
    public int occupiedNodes;

    protected void Awake()
    {
        localTargets = new List<GameObject>();
        targetsInRange = new List<GameObject>();
    }

    // Use this for initialization
    protected void Start () {
        DefaultValues();
        hp = maxhp;
        lifeTime = maxLifeTime;
        healthBarScale = healthBar.transform.localScale.y;
        StartCoroutine(EstablishAggro());
        StartCoroutine(AcquireTarget());
        assistTracker = new List<AI>();
        rbody = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
        lRenderer = GetComponent<LineRenderer>();
        lRenderer.SetVertexCount(0);
        switch (teamID)
        {
            case 0:
                lRenderer.SetColors(Color.green, Color.green);
                break;
            case 1:
                lRenderer.SetColors(Color.red, Color.red);
                break;
            default:
                lRenderer.SetColors(Color.blue, Color.blue);
                break;
        }
        lRenderer.sortingLayerName = "Default";
        lRenderer.sortingOrder = 9;
        activeEffects = new List<misc_Effect>();
        gameObject.layer = LayerMask.NameToLayer("Units");
        bodyGraphics.layer = LayerMask.NameToLayer("Units");
        healthBar.layer = LayerMask.NameToLayer("Units");
        mainGraph = (GridGraph)GameObject.FindGameObjectWithTag("Astar Path").GetComponent<AstarPath>().graphs[0];
        //if (targetNode == null) targetNode = NearestNode().gameObject;
        //temporary:
        if (teamID == 1) bodyGraphics.GetComponent<SpriteRenderer>().color = Color.red;
    }

    // Update is called once per frame
    protected void Update () {
        if (targetNode== null)
        {
            Debug.Log("targetNode is null -- attempting to default to nearest node");
            targetNode = NearestUnit(GameController.nodes);
            Debug.Log("targetNode is now " + targetNode.name + " for unit " + this.name);
        }
        lifeTime -= Time.deltaTime;                                                 //subtract from unit's lifetime
        GameController.FilterList(localTargets);                                    //filterlist() removes all null members of a list
        GameController.FilterList(targetsInRange);
        Vector3 newScale = new Vector3(1, healthBarScale * hp / maxhp, 1);          //Update healthbar size according to hp remaining
        healthBar.transform.localScale = newScale;
        if (currentNodeNodeControl != null)
        {
            if (currentNodeNodeControl.ownerTeamID != teamID || currentNodeNodeControl.ownership < 100)
            {
                isCapping = true;
            }
            else isCapping = false;
        }
        else
        {
            isCapping = false;
        }

        if (target == null) lRenderer.SetVertexCount(0);

        Debugger();
    }

    protected void LateUpdate()
    {
        if (hp < 0) OnDeath();
        if (lifeTime < 0) OnDeath();        //OnDeath() should only be called in LateUpdate
    }

    private void Debugger() 
    {
        if (this.destination != Vector3.zero)
        {
            Debug.DrawLine(this.transform.position, this.destination);
        }

        if(showDebugger) infoText.text = hp + "/" + maxhp;
    }

    private IEnumerator TempShowNodes(GridNode node)
    {
        node.Walkable = true;
        yield return new WaitForSeconds(1);
        node.Walkable = false;
    }

    /// <summary>
    /// Set all MP (multiplier) values to 1
    /// </summary>
    private void DefaultValues()
    {
        moveSpeedMP = 1;
        damageMP = 1;
        attackRangeMP = 1;
        acquisitionRangeMP = 1;
        RoFMP = 1;
    }

    /// <summary>
    /// Spawn an instance of prefab. This method assumes prefab will set its own base stats!
    /// </summary>
    /// <param name="spawnPoint"></param>
    /// <param name="targetNode"></param>
    /// <param name="prefab"></param>
    /// <param name="teamID"></param>
    /// <returns></returns>
    public static GameObject Spawn(
        Vector3 spawnPoint,
        NodeControl targetNode,
        GameObject prefab,
        int teamID)
    {
        GameObject newUnit = (GameObject)Instantiate(prefab, spawnPoint, Quaternion.identity);
        newUnit.GetComponent<AI>().teamID = teamID;
        newUnit.GetComponent<AI>().targetNode = targetNode.gameObject;
        GameController.teamLists[teamID].Add(newUnit);
        return newUnit;
    }

    /// <summary>
    /// Spawn an instance of gameobject called whatToSpawn
    /// </summary>
    /// <param name="spawnPoint"></param>
    /// <param name="moveSpeed"></param>
    /// <param name="damage"></param>
    /// <param name="attackRange"></param>
    /// <param name="acquisitionRange"></param>
    /// <param name="RoF"></param>
    /// <param name="maxhp"></param>
    /// <param name="detectionRange"></param>
    /// <param name="teamID"></param>
    /// <param name="maxLifeTime"></param>
    /// <param name="destination"></param>
    /// <param name="whatToSpawn"></param>
    /// <returns></returns>
    public static GameObject Spawn(
        Vector3 spawnPoint,
        float moveSpeedBase,
        float damageBase,
        float attackRangeBase,
        float acquisitionRangeBase,
        float RoFBase,
        float maxhp,
        float detectionRange,
        int teamID,
        float maxLifeTime,
        Vector3 destination,
        GameObject whatToSpawn)
    {
        GameObject newUnit = (GameObject)Instantiate(whatToSpawn, spawnPoint, Quaternion.identity);
        newUnit.GetComponent<AI>().moveSpeedBase = moveSpeedBase;
        newUnit.GetComponent<AI>().damageBase = damageBase;
        newUnit.GetComponent<AI>().attackRangeBase = attackRangeBase;
        newUnit.GetComponent<AI>().acquisitionRangeBase = acquisitionRangeBase;
        newUnit.GetComponent<AI>().RoFBase = RoFBase;
        newUnit.GetComponent<AI>().maxhp = maxhp;
        newUnit.GetComponent<AI>().detectionRange = detectionRange;
        newUnit.GetComponent<AI>().teamID = teamID;
        newUnit.GetComponent<AI>().maxLifeTime = maxLifeTime;
        newUnit.GetComponent<AI>().destination = destination;
        GameController.teamLists[teamID].Add(newUnit);
        return newUnit;
    }


    /// <summary>
    /// Clone this object with separate parameters
    /// </summary>
    /// <param name="spawnPoint"></param>
    /// <param name="moveSpeed"></param>
    /// <param name="damage"></param>
    /// <param name="attackRange"></param>
    /// <param name="acquisitionRange"></param>
    /// <param name="RoF"></param>
    /// <param name="maxhp"></param>
    /// <param name="detectionRange"></param>
    /// <param name="teamID"></param>
    /// <param name="maxLifeTime"></param>
    /// <param name="destination"></param>
    /// <returns></returns>
    public GameObject Spawn(
        Vector3 spawnPoint,
        float moveSpeedBase,
        float damageBase,
        float attackRangeBase,
        float acquisitionRangeBase,
        float RoFBase,
        float maxhp,
        float detectionRange,
        int teamID,
        float maxLifeTime,
        Vector3 destination)
    {
        GameObject newUnit = (GameObject)Instantiate(gameObject, spawnPoint, Quaternion.identity);
        newUnit.GetComponent<AI>().moveSpeedBase = moveSpeedBase;
        newUnit.GetComponent<AI>().damageBase = damageBase;
        newUnit.GetComponent<AI>().attackRangeBase = attackRangeBase;
        newUnit.GetComponent<AI>().acquisitionRangeBase = acquisitionRangeBase;
        newUnit.GetComponent<AI>().RoFBase = RoFBase;
        newUnit.GetComponent<AI>().maxhp = maxhp;
        newUnit.GetComponent<AI>().detectionRange = detectionRange;
        newUnit.GetComponent<AI>().teamID = teamID;
        newUnit.GetComponent<AI>().maxLifeTime = maxLifeTime;
        newUnit.GetComponent<AI>().destination = destination;
        //GameController.teamLists[teamID].Add(newUnit);
        return newUnit;
    }

    public virtual void OnHasCapped()
    {
        isCapping = false;
    }

    /// <summary>
    /// Move in the direction dir and orient graphics and colliders accordingly
    /// </summary>
    /// <param name="dir"></param>
    protected void Move(Vector3 dir)        
    {
        Orient(dir);
        this.transform.Translate(dir.normalized * moveSpeed * Time.deltaTime);
    }

    /// <summary>
    /// Move towards the point destination
    /// </summary>
    /// <param name="destination"></param>
    protected void MoveTo(Vector3 dest)  
    {
        if (Range(dest) > 0.1f)
        {
            Move(dest - this.transform.position);
        }
    }

    //DO NOT TOUCH, THIS TOOK FUCKING AGES
    /// <summary>
    /// Orient graphics and colliders according to the target field
    /// </summary>
    public void Orient()
    {
        float rotZ;
        if (this.target != null)
        {
            rotZ = Vector3.Angle(new Vector3(0, 1, 0), this.target.transform.position - this.bodyGraphics.transform.position);
            if (this.target.transform.position.x > this.bodyGraphics.transform.position.x) rotZ *= -1;
        }
        else rotZ = 0;
        this.bodyGraphics.transform.rotation = Quaternion.Euler(0, 0, rotZ);
    }

    /// <summary>
    /// Orient towards a specific direction
    /// </summary>
    /// <param name="dir"></param>
    public void Orient(Vector3 dir) 
    {
        float rotZ = Vector3.Angle(Vector3.up, dir);
        if (dir.x > 0) rotZ *= -1;
        this.bodyGraphics.transform.rotation = Quaternion.Euler(0, 0, rotZ);
    }

    /// <summary>
    /// Target acquisition process, to be repeated every .5 sec
    /// </summary>
    /// <returns></returns>
    protected IEnumerator AcquireTarget()
    {
        while (true)
        {
            Debug.Log("Acquiring targets...");
            this.target = null;
            Populate();     //populates the localTargets and targetsInRange lists through colliders
            //if no targets are in sight range, continue;
            if (this.localTargets.Count == 0)
            {
                yield return new WaitForSeconds(0.5f);  //after 0.5 seconds, the while(true) loop repeats
                continue;
            }
            else {
                //Target selection based on aggro (focus fire)
                if (this.selectByAggro)
                {
                    this.target = UnitWithHighestAggro();
                }

                //Target selection based on proximity
                else
                {
                    this.target = NearestEnemy();
                }
                yield return new WaitForSeconds(0.5f);  //after 0.5 seconds, the while(true) loop repeats
            }
        }
    }

    /// <summary>
    /// Automatically populates the lists globalTargets, localTargets, targetsInRange
    /// </summary>
    protected void Populate()
    {
        Debug.Log("Populating target list with ranges " + attackRange + " || " + acquisitionRange);
        localTargets = UnitsInRange(acquisitionRange);
        targetsInRange = UnitsInRange(attackRange);
    }

    private List<GameObject> UnitsInRange(float range)
    {
        List<GameObject> output = new List<GameObject>();
        Collider2D[] temp = Physics2D.OverlapCircleAll(this.transform.position, range, GameController.units);           //generate a list of all targets in attack range - possibly redundant?
        foreach (Collider2D coll in temp)
        {
            GameObject tempGO = coll.transform.parent.gameObject;
            AI tempAI = tempGO.GetComponent<AI>();
            if (tempAI != null)
            {
                if (tempAI.teamID != this.teamID && !tempAI.stealthed)
                {
                    output.Add(tempGO);
                }
            }
        }
        return output;
    }

    /// <summary>
    /// Returns the distance to the GameObject occupying the 'target' field
    /// </summary>
    /// <returns></returns>
    protected float Range()
    {
        //TODO: Investigate if it is worth it to use sqrMagnitude for better optimization
        if (this.target == null)
        {
            Debug.Log("Attempt to acces null object in AI.Range()");
            return 0;
        }
        return Range(this.target);
    }

    /// <summary>
    /// Returns the distance from the unit to t
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    protected float Range(GameObject t)
    {
        if (t != null) return (t.transform.position - this.transform.position).magnitude;
        if (t == null) Debug.Log("Attempt to acces null object in AI.Range()");
        return 0;
    }

    /// <summary>
    /// Returns the distance to some position
    /// </summary>
    /// <param name="position"></param>
    /// <returns></returns>
    protected float Range(Vector3 position)
    {
        return (position - this.transform.position).magnitude;
    }

    /// <summary>
    /// Returns the distance between two GameObjects
    /// </summary>
    /// <param name="obj1"></param>
    /// <param name="obj2"></param>
    /// <returns></returns>
    public static float Range(GameObject obj1, GameObject obj2)
    {
        return (obj1.transform.position - obj2.transform.position).magnitude;
    }

    //this is the attack function
    public virtual bool Engage()
    {
        Orient();
        //if target is out of range, move towards target and return false
        //TODO: determine range via collider, in case of large units
        if (Range() > attackRange)
        {
            Move(target.transform.position - this.transform.position);
            return false;
        }
        //Otherwise, deal damage
        else {
            if (!attackOnCoolDown) StartCoroutine(Attack());
            return true;
        }
    }

    //this is actually the attack function
    protected IEnumerator Attack()
    {
        if (!this.attackOnCoolDown && this.target!= null)
        {
            lRenderer.SetVertexCount(2);
            lRenderer.SetPosition(0, transform.position);
            lRenderer.SetPosition(1, target.transform.position);
            this.attackOnCoolDown = true;
            yield return new WaitForSeconds(attackAnimationDuration);
            this.target.GetComponent<AI>().OnReceiveDamage(damage, this);
            lRenderer.SetVertexCount(0);
            Debug.DrawLine(this.transform.position, this.target.transform.position, Color.red);
            yield return new WaitForSeconds(1 / RoF);
            this.attackOnCoolDown = false;
        }
    }

    //Aggro calculation
    //remember - Aggro is how enemies know who to attack, this value is only used by hostile units
    /// <summary>
    /// Dynamically calculates own unit's aggro
    /// </summary>
    /// <returns></returns>
    private IEnumerator EstablishAggro()
    {
        while (true)
        {
            aggro = 0;
            foreach (List<GameObject> list in GameController.teamLists)
            {
                foreach (GameObject unit in list) 
                {
                    AI temp = unit.GetComponent<AI>();
                    if (temp.teamID != teamID)
                    {
                        if (Range(unit) < temp.acquisitionRange)        //for each enemy unit, evaluate whether this unit is in its range
                        {                                               //establish aggro as the sum of all DPS (damage*RoF)
                            aggro += temp.damage * temp.RoF;
                        }
                    }
                }
            }
            aggro *= aggroMultiplier;                                   //include multiplier (if any)
            aggro = aggro * maxhp / hp;                                 //increase aggro if hp is low
            yield return new WaitForSeconds(0.5f);                      //iterate every 0.5 sec
        }
    }

    /// <summary>
    /// Returns the node that is on the optimal path from startingNode to finalNode. This is how units should select where to go next.
    /// </summary>
    /// <param name="startingNode"></param>
    /// <param name="finalNode"></param>
    /// <returns></returns>
    protected GameObject NodePicker(GameObject startingNode, GameObject finalNode)
    {
        if (startingNode == finalNode) return startingNode;
        List<GameObject> possibleNodes = startingNode.GetComponent<NodeControl>().linkedNodes;
        float minDistance = 1000000;
        GameObject tempNode = null;
        foreach (GameObject node in possibleNodes)
        {
            float distance = (node.transform.position - startingNode.transform.position).sqrMagnitude + NodeCrawler(node, finalNode);
            if (distance < minDistance)
            {
                minDistance = distance;
                tempNode = node;
            }
        }
        if (tempNode == null) Debug.LogError("nodePicker failure for finalnode " + finalNode.transform.name);
        return tempNode;
    }

    /// <summary>
    /// Recursion; travels the node net and returns the square length of the shortest path from startingNode to finalNode.
    /// ::: Do not call this directly - use NodeCrawler(GameObject startingNode, GameObject finalNode) instead!
    /// </summary>
    /// <param name="startingNode"></param>
    /// <param name="finalNode"></param>
    /// <param name="passedNodes"></param>
    /// <returns></returns>
    protected float NodeCrawler(GameObject startingNode, GameObject finalNode, List<GameObject> passedNodes)
    {
        passedNodes.Add(startingNode);
        List<GameObject> nodes = startingNode.GetComponent<NodeControl>().linkedNodes;
        float minDistance = 1000000;
        float f = 1000000;

        if (nodes.Contains(finalNode))
        {
            passedNodes.Add(finalNode);
            return (finalNode.transform.position - startingNode.transform.position).sqrMagnitude;
        }

        foreach (GameObject node in nodes)
        {
            if (!passedNodes.Contains(node))
            {
                f = (node.transform.position - startingNode.transform.position).sqrMagnitude + NodeCrawler(node, finalNode, passedNodes);
                if (f < minDistance)
                {
                    minDistance = f;
                }
            }
        }
        return f;
    }

    /// <summary>
    /// Recursion; travels the node net and returns the square length of the shortest path from startingNode to finalNode.
    /// </summary>
    /// <param name="startingNode"></param>
    /// <param name="finalNode"></param>
    /// <returns></returns>
    protected float NodeCrawler(GameObject startingNode, GameObject finalNode)
    {
        List<GameObject> newList = new List<GameObject>();
        newList.Add(startingNode);
        float t = NodeCrawler(startingNode, finalNode, newList);
        return t;
    }

    //Here are the OnTriggerEnter/Exit2D functions - 
    //for now they are only used to determine whether unit is within the capture area of a node
    //possibly use for a lot of other shit
    private void OnTriggerEnter2D(Collider2D coll)
    {
        //if (coll.gameObject.GetComponent<NodeControl>() != null)
        //{
        //    currentNode = coll.gameObject;
        //    currentNodeNodeControl = currentNode.GetComponent<NodeControl>();
        //    currentNodeNodeControl.unitsPresent.Add(gameObject);
        //}
    }

    private void OnTriggerExit2D(Collider2D coll)
    {
        //if (coll.gameObject.GetComponent<NodeControl>() != null)
        //{
        //    currentNode = null;
        //    currentNodeNodeControl = null;
        //    currentNodeNodeControl.unitsPresent.Remove(gameObject);
        //}
    }

    /// <summary>
    /// Returns a random point on a circle
    /// </summary>
    /// <param name="center"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static Vector3 RandomPointOnCircle(Vector3 center, float radius)
    {
        float x = 0;
        float y = 0;
        do
        {
            x = (Random.value - 0.5f) * 2 * radius;
            y = (Random.value - 0.5f) * 2 * radius;
        } while (Mathf.Pow(x, 2) + Mathf.Pow(y, 2) > Mathf.Pow(radius, 2));

        Vector3 newVector = new Vector3(center.x + x, center.y + y, 0);
        return newVector;
    }

    /// <summary>
    /// Returns a random pathfinding node in a given circle
    /// </summary>
    /// <param name="center"></param>
    /// <param name="radius"></param>
    /// <returns></returns>
    public static GraphNode RandomNodeOnCircle(Vector3 center, float radius)
    {
        //TODO: debug
        radius *= radius;
        List<GraphNode> elligible = new List<GraphNode>();
        foreach (GraphNode node in GameController.mainGraph.nodes)
        {
            if (((Vector3)node.position - center).sqrMagnitude < radius && node.Walkable==true)
            {
                elligible.Add(node);
            }
        }

        foreach(GraphNode node in elligible)
        {
                Debug.DrawLine(center, (Vector3)node.position, Color.green, 0.5f);

        }

        int index = (int)Random.Range(0, elligible.Count - 1);        
        return elligible[index];
    }

    /// <summary>
    /// Legacy code - use NodePicker(...) instead! Sets the destination to the linked node that is closest to finalTargetNode
    /// </summary>
    protected void NodeSelect()
    {
        if (finalTargetNode == null) return;
        GameObject tempTargetNode = null;
        if (currentNode != null)
        {
            if(currentNode.GetComponent<NodeControl>().ownerTeamID != teamID)
            {
                targetNode = currentNode;
                return;
            }
            float minDistance = (currentNode.transform.position - finalTargetNode.transform.position).magnitude;
            foreach (GameObject node in currentNode.GetComponent<NodeControl>().linkedNodes)
            {
                if ((node.transform.position - finalTargetNode.transform.position).magnitude < minDistance)
                {
                    tempTargetNode = node;
                    minDistance = (node.transform.position - finalTargetNode.transform.position).magnitude;
                }
            }
        }
        else
        {
            float minDistance = Range(GameController.nodes[0]);
            foreach(GameObject node in GameController.nodes)
            {
                if(Range(node) <= minDistance)
                {
                    minDistance = Range(node);
                    tempTargetNode = node;
                    Debug.Log("Distance from " + node.transform.name + " is " + Range(node) + "\n minDistance is " + minDistance);
                }
            }
        }
        if(tempTargetNode == null)
        {
            Debug.LogError("NodeSelect for " + transform.name + " has failed to locate a node. Terminating");
            return;
        }
        Debug.Log("setting targetNode to " + tempTargetNode.transform.name);
        targetNode = tempTargetNode;
    }

    /// <summary>
    /// Returns the point within the radius 'chkRadius' which will cover the highest number of friendly units with some buff
    /// </summary>
    /// <param name="center"></param>
    /// <param name="chkRadius"></param>
    /// <param name="buffRadius"></param>
    /// <param name="_teamID"></param>
    /// <returns></returns>
    public static Vector3 SelectDestination(Vector3 center, float chkRadius, float buffRadius, int _teamID)
    {
        //TODO: convert to static
        int count = 0;
        int maxCount = 0;
        Vector3 tempDestination = Vector3.zero;
        //create a field of discrete points in a local area
        for (float i = center.x - chkRadius; i < center.x + chkRadius; i ++)
        {
            for (float j = center.y - chkRadius; j < center.y + chkRadius; j ++)
            {
                count = 0;
                Collider2D[] temp = Physics2D.OverlapCircleAll(new Vector2(i, j), buffRadius);      //count how many friendlies in the area around each point

                foreach (Collider2D item in temp)
                {
                    AI tempAI = item.transform.parent.gameObject.GetComponent<AI>();
                    if (tempAI != null)
                    {
                        if (tempAI.teamID == _teamID && tempAI.damage * tempAI.RoF > 0) count++;    //only count attackers for now (possibly re-implement using layermask?)
                    }
                }
                if (count > maxCount)
                {
                    maxCount = count;
                    tempDestination = new Vector3(i, j, 0);                                         //select the point with the highest number of friendlies around it
                }
            }
        }
        if (maxCount > 1) return tempDestination;                                                   //return the position of that point only if there is at least 1 unit found, otherwise return vector3.zero
        else return Vector3.zero;
    }

    /// <summary>
    /// Returns the position of the nearest friendly offensive unit
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="teamID"></param>
    /// <returns></returns>
    public static Vector3 SelectDestinationGlobal(Vector3 origin, int teamID)
    {
        Vector3 temp = new Vector3(0, 0, 0);
        foreach (List<GameObject> list in GameController.teamLists)
        {
            if (list[0].GetComponent<AI>().teamID == teamID)
            {
                float minDistance = 1000;
                foreach (GameObject unit in list)
                {
                    if ((unit.transform.position - origin).magnitude < minDistance && unit.GetComponent<AI>().damage * unit.GetComponent<AI>().RoF > 0)
                    {
                        minDistance = (unit.transform.position - origin).magnitude;
                        temp = unit.transform.position;
                        Debug.DrawLine(origin, unit.transform.position);
                    }
                }
            }
        }
        return temp;
    }

    /// <summary>
    /// Returns all adjacent nodes such that unit is the only occupant
    /// </summary>
    /// <returns></returns>
    protected List<GridNode> GetSelfoccupiedNodes(AI unit)
    {
        int i = 0;
        List<GridNode> nodes = new List<GridNode>();
        List<GridNode> adjacentNodes = new List<GridNode>();
        foreach (GridNode node in mainGraph.nodes)
        {
            if (((Vector3)node.position - unit.transform.position).sqrMagnitude < mainGraph.nodeSize * mainGraph.nodeSize) adjacentNodes.Add(node);
        }
        foreach (GridNode node in adjacentNodes)
        {
            Vector3 center = (Vector3)node.position;
            Vector3 semidiagonal = new Vector3(mainGraph.nodeSize / 2, mainGraph.nodeSize / 2, 0);
            Collider2D[] objects = Physics2D.OverlapAreaAll(center - semidiagonal, center + semidiagonal, GameController.units);
            bool amPresent = false;
            bool othersPresent = false;
            foreach (Collider2D col in objects)
            {
                if (col.transform.parent.gameObject.GetComponent<AI>() == this) amPresent = true;
                else othersPresent = true;
            }
            if (i > 3)
            {
                Debug.LogError("GetSelfOccupiedNodes has exceeded 4 nodes limit - aborting (unit: " + unit.name + ")");
                return null;
            }
            if (amPresent && !othersPresent) nodes.Add(node);
        }
        unit.occupiedNodes = nodes.Count;
        return nodes;
    }

    protected List<GridNode> GetSelfoccupiedNodes2(AI unit)
    {
        int i = 0;
        List<GridNode> nodes = new List<GridNode>();
        List<GridNode> adjacentNodes = new List<GridNode>();
        foreach (GridNode node in mainGraph.nodes)
        {
            if (((Vector3)node.position - unit.transform.position).sqrMagnitude < mainGraph.nodeSize * mainGraph.nodeSize) adjacentNodes.Add(node);
        }
        foreach (GridNode node in adjacentNodes)
        {
            Vector3 center = (Vector3)node.position;
            Vector3 semidiagonal = new Vector3(mainGraph.nodeSize / 2, mainGraph.nodeSize / 2, 0);
            Collider2D[] objects = Physics2D.OverlapAreaAll(center - semidiagonal, center + semidiagonal, GameController.units);
            bool amPresent = false;
            foreach (Collider2D col in objects)
            {
                if (col.transform.parent.gameObject.GetComponent<AI>() == unit) amPresent = true;
            }
            if (i > 3)
            {
                Debug.LogError("GetSelfOccupiedNodes2 has exceeded 4 nodes limit - aborting (unit: " + unit.name + ")");
                return null;
            }
            if (amPresent) nodes.Add(node);
        }
        unit.occupiedNodes = nodes.Count;
        return nodes;
    }

    /// <summary>
    /// Returns all adjacet nodes such that this unit is the only occupant
    /// </summary>
    /// 
    /// <returns></returns>
    protected List<GridNode> GetSelfoccupiedNodes()
    {
        return GetSelfoccupiedNodes(this);
    }

    /// <summary>
    /// Returns a list of all nodes occupied by units other than [unit]
    /// </summary>
    /// <param name="unit"></param>
    /// <returns></returns>
    protected List<GridNode> GetOccupiedNodes(AI unit)
    {
        List<GridNode> oNodes = new List<GridNode>();
        foreach(List<GameObject> list in GameController.teamLists)
        {
            foreach(GameObject gObject in list)
            {
                if (this.gameObject == gObject) continue;
                foreach(GridNode node in GetSelfoccupiedNodes2(gObject.GetComponent<AI>()))
                {
                    oNodes.Add(node);
                }
            }
        }
        return oNodes;
    }

    protected List<GridNode> GetOccupiedNodes()
    {
        return GetOccupiedNodes(this);
    }

    /// <summary>
    /// Clears self occupied nodes, then rescans the grid after t seconds
    /// </summary>
    /// <param name="t"></param>
    /// <returns></returns>
    protected IEnumerator ForceClearSelfoccupiedCR(float t)
    {
        ForceClearSelfoccupied();
        yield return new WaitForSeconds(t);
        GameController.GridScan();
    }

    /// <summary>
    /// Clears self occupied nodes
    /// </summary>
    protected void ForceClearSelfoccupied()
    {
        foreach (GridNode node in GetSelfoccupiedNodes())
        {
            node.Walkable = true;
        }
    }

    /// <summary>
    /// Sets all nodes occupied by other units to unwalkable
    /// </summary>
    protected void ForceSetOccupiedUnwalkable()
    {
        foreach(GridNode node in GetOccupiedNodes())
        {
            node.Walkable = false;
        }
    }

    /// <summary>
    /// Returns the nearest enemy within acquisitionRange; null if none are present
    /// </summary>
    /// <returns></returns>
    protected GameObject NearestEnemy()
    {
        if (localTargets.Count == 0)
        {
            return null;
        }

        float minRange = Range(localTargets[0]);
        GameObject tempTarget = localTargets[0];
        foreach (GameObject item in localTargets)
        {
            if (Range(item) < minRange)
            {
                minRange = Range(item);
                tempTarget = item;
            }
        }
        return tempTarget;
        return null;
    }

    /// <summary>
    /// Returns the nearest unit that is a member of unitList
    /// </summary>
    /// <param name="list"></param>
    /// <returns></returns>
    protected GameObject NearestUnit(List<GameObject> unitList)
    {
        if (unitList.Count == 0)
        {
            Debug.LogError("NearestUnit called on empty list");
            return null;
        }
        else {
            float minRange = Range(unitList[0]);
            GameObject tempTarget = unitList[0];
            foreach (GameObject item in unitList)
            {
                if (Range(item) < minRange)
                {
                    minRange = Range(item);
                    tempTarget = item;
                }
            }
            Debug.Log("minimum distance: " + minRange);
            return tempTarget;
        }
    }

    /// <summary>
    /// Returns the nearest unit in unitList of type T
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="unitList"></param>
    /// <returns></returns>
    protected GameObject NearestUnit<T>(List<GameObject> unitList)
    {
        float minRange = 1000;
        GameObject tempTarget = null;
        foreach (GameObject item in unitList)
        {
            if (Range(item) < minRange && item.GetComponent<T>() != null)
            {
                minRange = Range(item);
                tempTarget = item;
            }
        }
        return tempTarget;
    }

    /// <summary>
    /// Returns the nearest enemy of type T in unitList
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="unitList"></param>
    /// <returns></returns>
    protected GameObject NearestEnemy<T>(List<GameObject> unitList)
    {
        float minRange = 1000;
        GameObject tempTarget = null;
        foreach (GameObject item in unitList)
        {
            if (Range(item) < minRange && item.GetComponent<T>() != null && item.GetComponent<AI>().teamID != teamID)
            {
                minRange = Range(item);
                tempTarget = item;
            }
        }
        return tempTarget;
    }

    /// <summary>
    /// Returns the GraphNode nearest to [position]
    /// </summary>
    /// <param name="position"></param>
    /// <param name="graph"></param>
    /// <returns></returns>
    protected GridNode NearestGraphNode(Vector3 position, GridGraph graph)
    {
        float minDist = ((Vector3)graph.nodes[0].position-position).sqrMagnitude;
        GridNode mNode = graph.nodes[0];
        foreach(GridNode node in graph.nodes)
        {
            if (((Vector3)node.position - position).sqrMagnitude < minDist)
            {
                minDist = ((Vector3)node.position - position).sqrMagnitude;
                mNode = node;
            }
        }
        Debug.DrawLine(position, (Vector3)mNode.position, Color.red, 1f);
        return mNode;
    }

    /// <summary>
    /// Returns the GraphNode nearest to this unit
    /// </summary>
    /// <returns></returns>
    protected GraphNode NearestGraphNode()
    {
        return NearestGraphNode(transform.position, mainGraph);
    }

    /// <summary>
    /// Returns the enemy with the highest aggro value out of all enemies in acquisitionRange
    /// </summary>
    /// <returns></returns>
    protected GameObject UnitWithHighestAggro()
    {
        GameObject tempTarget = null;
        float aggroMax = 0;
        foreach (GameObject item in localTargets)
        {
            if (item.GetComponent<AI>().aggro >= aggroMax && item.GetComponent<AI>() != null)
            {
                aggroMax = item.GetComponent<AI>().aggro;
                tempTarget = item;
            }
        }
        return tempTarget;
    }
   
    /// <summary>
    /// Returns the node nearest to this unit
    /// </summary>
    /// <returns></returns>
    protected NodeControl NearestNode()
    {
        if (GameController.nodes.Count == 0)
        {
            Debug.LogError("AI.NearestNode() will not work -- GameController detects 0 nodes!");
            return null;
        }
        else
        {
            float minDist = Range(GameController.nodes[0]);
            NodeControl tempNode = GameController.nodes[0].GetComponent<NodeControl>();
            foreach(GameObject node in GameController.nodes)
            {
                if (Range(node) < minDist)
                {
                    minDist = Range(node);
                    tempNode = node.GetComponent<NodeControl>();
                }
            }
            return tempNode;
        }
    }

    public static NodeControl NearestNode(Vector3 position)
    {
        if (GameController.nodes.Count == 0)
        {
            Debug.LogError("AI.NearestNode() will not work -- GameController detects 0 nodes!");
            return null;
        }
        else
        {
            float minDist = (GameController.nodes[0].transform.position-position).sqrMagnitude;
            NodeControl tempNode = GameController.nodes[0].GetComponent<NodeControl>();
            foreach (GameObject node in GameController.nodes)
            {
                float currentDist = (node.transform.position - position).sqrMagnitude;
                Debug.Log("sqrDistance = " + currentDist + "; minDist = " + minDist + "; checking: " + node.name);
                if (currentDist < minDist)
                {
                    minDist = currentDist;
                    tempNode = node.GetComponent<NodeControl>();
                }
            }
            return tempNode;
        }

    }

    /// <summary>
    /// Returns "unit" if object is a unit, "node" if object is a node
    /// </summary>
    /// <param name="target"></param>
    /// <returns></returns>
    public static string GetTypeString(GameObject target)
    {
        if (target.GetComponent<AI>() != null) return "unit";
        if (target.GetComponent<NodeControl>() != null) return "node";
        else return null;
    }

    /// <summary>
    /// Called whenever this unit kills an enemy
    /// </summary>
    /// <param name="unitKilled"></param>
    public virtual void KillConfirmed(AI unitKilled)
    {
        killTracker.Add(unitKilled);
    }

    /// <summary>
    /// Inflicts damage to the parent unit
    /// </summary>
    /// <param name="damageDone"></param>
    public void OnReceiveDamage(float damageDone)
    {
        if(!immune) this.hp -= damageDone;
        Debug.Log(transform.name + "is hit for " + damageDone + " damage");
    }

    public void OnReceiveDamage(float damageDone, AI damagedealer)
    {
        //put enemy at the top of the assistTracker
        //assistTracker doesn't work - figure out how to store unit ghosts
        assistTracker.Remove(damagedealer);
        assistTracker.Add(damagedealer);

        if (!immune) this.hp -= damageDone;
        Debug.Log(transform.name + "is hit for " + damageDone + " damage");
    }
     
    /// <summary>
    /// Kill the unit
    /// </summary>
    protected virtual void OnDeath()
    {
        Debug.Log("Initiating OnDeath() for " + name);

        deathLog log = new deathLog();

        log.name = this.name;
        log.type = this.type;
        log.maxhp = this.maxhp;
        log.maxLifeTime = this.maxLifeTime;
        log.teamID = this.teamID;

        log.positionOnDeath = transform.position;
        log.hpOnDeath = Mathf.Clamp(hp, 0, Mathf.Infinity);
        log.lifeTimeOnDeath = Mathf.Clamp(this.lifeTime, 0, Mathf.Infinity);
        log.killedBy = (log.hpOnDeath > 0 ? null : assistTracker[assistTracker.Count - 1]);
        if(assistTracker.Count>0) assistTracker.RemoveAt(assistTracker.Count - 1);
        log.assists = assistTracker.ToArray();
        Debug.Log("position1");
        if (log.killedBy != null) log.killedBy.KillConfirmed(this);
        Debug.Log("position2");
        log.timeOfDeath = Time.timeSinceLevelLoad;
        GameController.killBoard.Add(log);
        GameController.teamLists[teamID].Remove(gameObject);

        Debug.Log(transform.name + " is destroyed");
        Destroy(gameObject);
    }

    private void OnMouseEnter()
    {
        GameController.activeUnit = this;
    }
}