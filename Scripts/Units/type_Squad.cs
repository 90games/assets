﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class type_Squad : behavior_Default {

    public type_Squad squadCommander;
    public List<type_Squad> squadMembers = new List<type_Squad>();

    // Use this for initialization
    new void Start () {
        base.Start();
        if (squadCommander != null)
        {
            Debug.Log("I have a commander, so I will add myself to his list!");
            Debug.Log("adding " + name + " to subUnits list of " + squadCommander.name);
            squadCommander.squadMembers.Add(this);
        }
        else if (squadMembers.Count > 0)
        {
            foreach (type_Squad unit in squadMembers)
            {
                if (unit.squadCommander != this)
                {
                    Debug.Log("Unit " + unit.name + " is being insubordinate! I will add myself as his squadCommander.");
                    unit.squadCommander = this;
                }
            }
        }
        else Debug.Log("Unit " + name + " has spawned outside of squad. Error?");
    }

    // Update is called once per frame
    new void Update () {
        base.Update();
        if (squadCommander != null)
        {
            this.target = squadCommander.target;
            this.destination = squadCommander.destination;
        }
        else foreach (type_Squad squadMember in squadMembers) Debug.DrawLine(transform.position, squadMember.transform.position);
        filterMembers();
    }

    private void filterMembers()
    {
        for (int i = 0; i < squadMembers.Count; i++) {
            if (squadMembers[i] == null) squadMembers.RemoveAt(i--);
        }
    }

    protected override void OnDeath()
    {
        GameController.FilterList(squadMembers);
        if (squadCommander == null && squadMembers.Count > 0)
        {
            Debug.Log("hello");
            squadMembers[0].squadMembers = squadMembers;
            squadMembers[0].squadMembers.RemoveAt(0);
            foreach (type_Squad unit in squadMembers[0].squadMembers)
            {
                unit.squadCommander = squadMembers[0];
            }
        }
        base.OnDeath();
    }
}
