﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//TODO: implement squad mechanics
public class unit_Baneling10 : type_Squad {

    public float explosionRadius;

    [Tooltip("The distance at which the baneling will detonate")]
    public float detonateDistance;

    private bool rush = false;
    public GameObject explosionPrefab;

    // Use this for initialization
    new void Start () {
        attackRangeBase = 0;
        type = "Baneling";
        base.Start();
        if (Range(targetNode) < acquisitionRange && localTargets.Count == 0) target = targetNode; 
    }

    // Update is called once per frame
    new void Update () {
        base.Update();

        //if (destination == Vector3.zero)
        //{
        //    Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0)!");
        //    this.destination = RandomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
        //}

        if (this.target != null)
        {
            Engage();
        }
	}

    public override bool Engage()
    {
        destination = target.transform.position;
        if (!base.Engage())
        {
            if (!rush)
            {
                moveSpeedMP += 2;
                rush = true;
            }
            return false;
        }
        if (Range() < detonateDistance)
        {
            detonate();
        }
        return false;
    }

    private void OnCollisionEnter2D(Collision2D coll)
    {

        if (coll.gameObject == target || coll.transform.parent.gameObject== target)
        {
            Debug.Log("Dealing damage and suiciding now");
            detonate();
        }
    }

    private void detonate()
    {
        GameObject newSplosion = (GameObject)Instantiate(explosionPrefab, transform.position, Quaternion.identity);
        explosion newExplosion = newSplosion.GetComponent<explosion>();
        newExplosion.radius = this.explosionRadius;
        newExplosion.teamID = this.teamID;
        newExplosion.damage = this.damage;
        newExplosion.friedlyFire = false;
        Debug.Log(name + " suiciding");
        OnDeath();
        Debug.LogError("Why is this unit still alive after calling OnDeath()?");
        Destroy(gameObject);
    }
}
