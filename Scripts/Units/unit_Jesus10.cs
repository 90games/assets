﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class unit_Jesus10 : behavior_Default
{

    [Tooltip("How long this Jesus will stay dead")]
    public float rezTimer = 1;

    public bool hasDied = false;

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        //if (destination == Vector3.zero)
        //{
        //    Debug.LogError("Destination of unit " + transform.name + " set to (0, 0, 0)!");
        //    this.destination = RandomPointOnCircle(targetNode.transform.position, targetNode.GetComponent<NodeControl>().capRadius);
        //}
    }

    protected override void OnDeath()
    {
        Debug.Log("Successfully overriden base.OnDeath()");
        if (!this.hasDied)
        {
            //StartCoroutine(resurrection());
            GameObject.FindGameObjectWithTag("GameController").AddComponent<unit_Jesus_ResurrectionScript>();
            unit_Jesus_ResurrectionScript rezScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<unit_Jesus_ResurrectionScript>();
            rezScript.teamID = this.teamID;
            rezScript.resurrectionDelay = this.rezTimer;
            rezScript.targetNode = this.targetNode.GetComponent<NodeControl>();
            rezScript.spawnPosition = transform.position;
            GameController.teamLists[teamID].Remove(gameObject);
            Destroy(gameObject);
        }
        else base.OnDeath();
    }
}