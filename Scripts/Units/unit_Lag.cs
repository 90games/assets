﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class unit_Lag : behavior_Default
{

    public float lagDelay;
    public float lagDuration;
    public float lagFactor;
    public float lagRadius;

    // Use this for initialization
    new void Start()
    {
        Debug.Log("Ping1");
        base.Start();
        StartCoroutine(applyLag());
        Debug.Log("Ping3");
    }

    private IEnumerator applyLag()
    {
        Debug.Log("Ping2");
        yield return new WaitForSeconds(lagDelay);
        Debug.Log("Casting Lag");
        Collider2D[] cols = Physics2D.OverlapCircleAll(transform.position, lagRadius, GameController.units);
        foreach (Collider2D col in cols)
        {
            if (col.transform.parent.GetComponent<effect_debuff_Lag>() == null && col.transform.parent.GetComponent<AI>().teamID != this.teamID)
            {
                effect_debuff_Lag lag = col.transform.parent.gameObject.AddComponent<effect_debuff_Lag>();
                lag.effectDuration = lagDuration;
                lag.effectOnset = 0;
                lag.lagFactor = this.lagFactor;
            }
        }
    }
}